# Flask-SQL-Alchemy-APY



## About

A blueprint startpoint for a API Server with a Database.

- Flask server
- SQLite database
- SQLAlchemy database interaction
- Marshmallow object (de-)serialization